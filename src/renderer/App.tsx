// styles
import './AppStyles.scss';
import 'bootstrap/dist/css/bootstrap.min.css';
import './lite.css';

import AppRouter from './AppRouter';

export default () => {
  return <AppRouter />;
};
