import { action, makeObservable, observable } from 'mobx';

export interface TaskItem {
  id: string;
  options: any;
  code: string;
  description: string;
  name: string;
  docs: string;
  url?: string;
  retries?: string;
  error?: string;
  screenshot?: string;
  visible?: string;
  timeout?: string;
  method?: string;
  status?: string;
}

export interface WorkFlowItem {
  id: string;
  createdAt: string;
  image: string;
  description: string;
  docs: string;
  tasks: TaskItem[];
  options: object;
  name: string;
}

export class WorkflowStoreImplementation {
  workflows: WorkFlowItem[] = [];

  constructor() {
    makeObservable(this, {
      workflows: observable,
      addWorkFlow: action,
      addTaskToWorkFlow: action,
      getWorkflows: action,
    });
  }

  getWorkflows() {
    return this.workflows;
  }

  addWorkFlow(workflow: WorkFlowItem) {
    this.workflows.push(workflow);
  }

  setWorkFlow(workflows: WorkFlowItem[]) {
    this.workflows = workflows;
  }

  addTaskToWorkFlow(workflowID: string, aTaskItem: TaskItem) {
    const workflowIndex = this.workflows.findIndex(
      (el) => el.id === workflowID
    );
    if (workflowIndex !== -1) {
      this.workflows[workflowIndex].tasks.push(aTaskItem);
    }
  }
}

export const WorkflowStore = new WorkflowStoreImplementation();
