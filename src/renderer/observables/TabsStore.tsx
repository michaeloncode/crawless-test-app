import { action, makeObservable, observable } from 'mobx';

interface TabItem {
  id: string;
  type: string;
  //Icon: any;
  title: string;
}

export class TabStoreImplementation {
  tabs: TabItem[] = [];

  activeTab: string = '';

  constructor() {
    makeObservable(this, {
      tabs: observable,
      activeTab: observable,
      addorActivateTab: action,
      removeTab: action,
      setActiveTab: action,
      getActiveTab: action,
      getAllTabs: action,
    });
  }

  setActiveTab(tabID: string) {
    this.activeTab = tabID;
  }

  getActiveTab() {
    return this.activeTab;
  }

  getAllTabs() {
    return this.tabs;
  }

  addorActivateTab(tab: TabItem) {
    // find if tab exist and if exist, set as active
    const tabIndex = this.tabs.findIndex((el) => el.id === tab.id);
    if (tabIndex !== -1) {
      if (tab.id !== this.getActiveTab()) {
        this.setActiveTab(tab.id);
      }
    } else {
      this.tabs.push(tab);
      this.setActiveTab(tab.id);
    }
  }

  removeTab(tabID: String) {
    const tabIndex = this.tabs.findIndex((el) => el.id === tabID);
    if (tabIndex !== -1) {
      this.tabs.splice(tabIndex, 1);
    }
  }
}

export const TabStore = new TabStoreImplementation();
