import { Svg } from '@crawless/ui/lite';
import { Colors } from 'renderer/theme';

export const FIREBASE_STORAGE = 'FIREBASE_STORAGE';

export const MONGO_STORAGE = 'MONGO_STORAGE';

export const WORKFLOW = 'WORKFLOW';
export const WORKFLOW_TASK_GROUP = 'WORKFLOW_TASK_GROUP';
export const WORKFLOW_TASK = 'WORKFLOW_TASK';

export const OTHERS = 'OTHERS';

export const projects = [
  {
    id: 'project1',
    title: 'PROJECT NAME',
    isRunning: false,
    subs: [
      {
        id: 'firebase',
        type: FIREBASE_STORAGE,
        Icon: Svg.Firebase,
        title: 'Storage',
        subs: [],
      },
      {
        id: 'mongodb',
        type: MONGO_STORAGE,
        Icon: Svg.Mongo,
        title: 'Storage',
        subs: [
          {
            id: '5',
            Icon: Svg.Collection,
            title: 'Collection',
          },
          {
            id: '6',
            Icon: Svg.Collection,
            title: 'Collection',
          },
          {
            id: '7',
            Icon: Svg.Collection,
            title: 'Collection',
          },
          {
            id: '8',
            Icon: Svg.Collection,
            title: 'Collection',
          },
        ],
      },
      {
        id: 'workflow1',
        type: WORKFLOW,
        Icon: Svg.Workflow,
        title: 'Workflow',
        subs: [],
      },
      {
        id: 'workflow2',
        type: WORKFLOW,
        Icon: Svg.Workflow,
        title: 'Workflow',
        subs: [],
      },
      {
        id: 'workflow3',
        type: WORKFLOW,
        Icon: Svg.Workflow,
        title: 'Workflow',
        subs: [
          {
            id: '12',
            Icon: Svg.TaskMono,
            title: 'Task',
            type: WORKFLOW_TASK,
            color: Colors.yellow50,
          },
          {
            id: '13',
            Icon: Svg.TaskMono,
            title: 'get_input_data',
            type: WORKFLOW_TASK,
          },
          {
            id: '14',
            Icon: Svg.TaskMono,
            title: 'details',
            type: WORKFLOW_TASK,
          },
          {
            id: '15',
            Icon: Svg.TaskMono,
            title: 'settings',
            type: WORKFLOW_TASK,
          },
        ],
      },
      {
        id: '16',
        type: WORKFLOW,
        Icon: Svg.Workflow,
        title: 'Workflow',
        subs: [
          {
            id: '17',
            Icon: Svg.Folder,
            title: 'Group of Tasks',
            type: WORKFLOW_TASK_GROUP,
            tasks: [],
          },
          {
            id: '18',
            Icon: Svg.Folder,
            title: 'Group of Tasks',
            type: WORKFLOW_TASK_GROUP,
            tasks: [
              {
                id: '19',
                Icon: Svg.TaskMono,
                title: 'deals_of_the_week',
                type: WORKFLOW_TASK,
              },
              {
                id: '20',
                Icon: Svg.TaskMono,
                title: 'facebook_login',
                type: WORKFLOW_TASK,
              },
            ],
          },
          {
            id: '21',
            Icon: Svg.TaskMono,
            title: 'Task',
            type: WORKFLOW_TASK,
            color: Colors.yellow50,
          },
          {
            id: '22',
            Icon: Svg.TaskMono,
            title: 'form_submit',
            type: WORKFLOW_TASK,
          },
        ],
      },
    ],
  },
  {
    id: 'project2',
    title: 'PROJECT NAME',
    isRunning: true,
    subs: [
      {
        id: 'firebase',
        type: FIREBASE_STORAGE,
        Icon: Svg.Firebase,
        title: 'Storage',
        subs: [],
      },
      {
        id: 'mongodb',
        type: MONGO_STORAGE,
        Icon: Svg.Mongo,
        title: 'Storage',
        subs: [
          {
            id: '5',
            Icon: Svg.Collection,
            title: 'Collection',
          },
          {
            id: '6',
            Icon: Svg.Collection,
            title: 'Collection',
          },
          {
            id: '7',
            Icon: Svg.Collection,
            title: 'Collection',
          },
          {
            id: '8',
            Icon: Svg.Collection,
            title: 'Collection',
          },
        ],
      },
      {
        id: 'workflow1',
        type: WORKFLOW,
        Icon: Svg.Workflow,
        title: 'Workflow',
        subs: [],
      },
      {
        id: 'workflow2',
        type: WORKFLOW,
        Icon: Svg.Workflow,
        title: 'Workflow',
        subs: [],
      },
      {
        id: 'workflow3',
        type: WORKFLOW,
        Icon: Svg.Workflow,
        title: 'Workflow',
        subs: [
          {
            id: '12',
            Icon: Svg.TaskMono,
            title: 'Task',
            type: WORKFLOW_TASK,
            color: Colors.yellow50,
          },
          {
            id: '13',
            Icon: Svg.TaskMono,
            title: 'get_input_data',
            type: WORKFLOW_TASK,
          },
          {
            id: '14',
            Icon: Svg.TaskMono,
            title: 'details',
            type: WORKFLOW_TASK,
          },
          {
            id: '15',
            Icon: Svg.TaskMono,
            title: 'settings',
            type: WORKFLOW_TASK,
          },
        ],
      },
      {
        id: '16',
        type: WORKFLOW,
        Icon: Svg.Workflow,
        title: 'Workflow',
        subs: [
          {
            id: '17',
            Icon: Svg.Folder,
            title: 'Group of Tasks',
            type: WORKFLOW_TASK_GROUP,
            tasks: [],
          },
          {
            id: '18',
            Icon: Svg.Folder,
            title: 'Group of Tasks',
            type: WORKFLOW_TASK_GROUP,
            tasks: [
              {
                id: '19',
                Icon: Svg.TaskMono,
                title: 'deals_of_the_week',
                type: WORKFLOW_TASK,
              },
              {
                id: '20',
                Icon: Svg.TaskMono,
                title: 'facebook_login',
                type: WORKFLOW_TASK,
              },
            ],
          },
          {
            id: '21',
            Icon: Svg.TaskMono,
            title: 'Task',
            type: WORKFLOW_TASK,
            color: Colors.yellow50,
          },
          {
            id: '22',
            Icon: Svg.TaskMono,
            title: 'form_submit',
            type: WORKFLOW_TASK,
          },
        ],
      },
    ],
  },
  {
    id: '23',
    title: 'FVV',
    isRunning: false,
    subs: [
      {
        id: 'firebase',
        type: FIREBASE_STORAGE,
        Icon: Svg.Firebase,
        title: 'Storage',
        subs: [],
      },
      {
        id: 'mongodb',
        type: MONGO_STORAGE,
        Icon: Svg.Mongo,
        title: 'Storage',
        subs: [
          {
            id: '5',
            Icon: Svg.Collection,
            title: 'Collection',
          },
          {
            id: '6',
            Icon: Svg.Collection,
            title: 'Collection',
          },
          {
            id: '7',
            Icon: Svg.Collection,
            title: 'Collection',
          },
          {
            id: '8',
            Icon: Svg.Collection,
            title: 'Collection',
          },
        ],
      },
      {
        id: 'workflow1',
        type: WORKFLOW,
        Icon: Svg.Workflow,
        title: 'Workflow',
        subs: [],
      },
      {
        id: 'workflow2',
        type: WORKFLOW,
        Icon: Svg.Workflow,
        title: 'Workflow',
        subs: [],
      },
      {
        id: 'workflow3',
        type: WORKFLOW,
        Icon: Svg.Workflow,
        title: 'Workflow',
        subs: [
          {
            id: '12',
            Icon: Svg.TaskMono,
            title: 'Task',
            type: WORKFLOW_TASK,
            color: Colors.yellow50,
          },
          {
            id: '13',
            Icon: Svg.TaskMono,
            title: 'get_input_data',
            type: WORKFLOW_TASK,
          },
          {
            id: '14',
            Icon: Svg.TaskMono,
            title: 'details',
            type: WORKFLOW_TASK,
          },
          {
            id: '15',
            Icon: Svg.TaskMono,
            title: 'settings',
            type: WORKFLOW_TASK,
          },
        ],
      },
      {
        id: '16',
        type: WORKFLOW,
        Icon: Svg.Workflow,
        title: 'Workflow',
        subs: [
          {
            id: '17',
            Icon: Svg.Folder,
            title: 'Group of Tasks',
            type: WORKFLOW_TASK_GROUP,
            tasks: [],
          },
          {
            id: '18',
            Icon: Svg.Folder,
            title: 'Group of Tasks',
            type: WORKFLOW_TASK_GROUP,
            tasks: [
              {
                id: '19',
                Icon: Svg.TaskMono,
                title: 'deals_of_the_week',
                type: WORKFLOW_TASK,
              },
              {
                id: '20',
                Icon: Svg.TaskMono,
                title: 'facebook_login',
                type: WORKFLOW_TASK,
              },
            ],
          },
          {
            id: '21',
            Icon: Svg.TaskMono,
            title: 'Task',
            type: WORKFLOW_TASK,
            color: Colors.yellow50,
          },
          {
            id: '22',
            Icon: Svg.TaskMono,
            title: 'form_submit',
            type: WORKFLOW_TASK,
          },
        ],
      },
    ],
  },
];

export const DEFAULT_TABS = [
  {
    id: '1',
    Icon: Svg.Crawless,
    title: 'Welcome',
    type: OTHERS,
  },
  {
    id: '2',
    Icon: Svg.Dashboard,
    title: 'Statistics',
    type: OTHERS,
  },
  {
    id: '3',
    Icon: Svg.TaskMono,
    title: 'Task',
    type: WORKFLOW_TASK,
    color: Colors.yellow50,
  },
  {
    id: '4',
    Icon: Svg.Workflow,
    title: 'Workflow',
    type: WORKFLOW,
  },
  {
    id: '5',
    Icon: Svg.Collection,
    title: 'Collection',
    type: OTHERS,
  },
  {
    id: '6',
    Icon: Svg.Mongo,
    title: 'Storage',
    type: MONGO_STORAGE,
  },
];

export const workflowTasksData = [
  {
    id: '1',
    status: 'waiting',
    createdAt: new Date(),
    name: 'taskname',
    url: 'https://www.statista.com',
    retries: 2,
    error: 'Error message text...',
    screenshot:
      'https://animashaunmichael.com/wp-content/uploads/2022/05/favicon001-cropped.png',
    visible: false,
    timeout: '3600', // seconds
    method: 'put',
  },
  {
    id: '2',
    status: 'waiting',
    createdAt: new Date(),
    name: 'taskname',
    url: 'https://www.statista.com',
    retries: 2,
    error: '',
    screenshot:
      'https://animashaunmichael.com/wp-content/uploads/2022/05/favicon001-cropped.png',
    visible: true,
    timeout: '3600', // seconds
    method: 'get',
  },
  {
    id: '3',
    status: 'waiting',
    createdAt: new Date(),
    name: 'taskname',
    url: 'https://www.statista.com',
    retries: 2,
    error: '',
    screenshot:
      'https://animashaunmichael.com/wp-content/uploads/2022/05/favicon001-cropped.png',
    visible: true,
    timeout: '3600', // seconds
    method: 'get',
  },
  {
    id: '4',
    status: 'completed',
    createdAt: new Date(),
    name: 'taskname',
    url: 'https://www.statista.com',
    retries: 2,
    error: 'Error message text...',
    screenshot:
      'https://animashaunmichael.com/wp-content/uploads/2022/05/favicon001-cropped.png',
    visible: true,
    timeout: '3600', // seconds
    method: 'head',
  },
  {
    id: '5',
    status: 'waiting',
    createdAt: new Date(),
    name: 'taskname',
    url: 'https://www.statista.com',
    retries: 2,
    error: 'Error message text...',
    screenshot:
      'https://animashaunmichael.com/wp-content/uploads/2022/05/favicon001-cropped.png',
    visible: false,
    timeout: '3600', // seconds
    method: 'delete',
  },
  {
    id: '6',
    status: 'active',
    createdAt: new Date(),
    name: 'taskname',
    url: 'https://www.statista.com',
    retries: 2,
    error: '',
    screenshot:
      'https://animashaunmichael.com/wp-content/uploads/2022/05/favicon001-cropped.png',
    visible: true,
    timeout: '3600', // seconds
    method: 'get',
  },
  {
    id: '7',
    status: 'active',
    createdAt: new Date(),
    name: 'taskname',
    url: 'https://www.statista.com',
    retries: 2,
    error: '',
    screenshot:
      'https://animashaunmichael.com/wp-content/uploads/2022/05/favicon001-cropped.png',
    visible: true,
    timeout: '3600', // seconds
    method: 'get',
  },
  {
    id: '8',
    status: 'completed',
    createdAt: new Date(),
    name: 'taskname',
    url: 'https://www.statista.com',
    retries: 2,
    error: 'Error message text...',
    screenshot:
      'https://animashaunmichael.com/wp-content/uploads/2022/05/favicon001-cropped.png',
    visible: true,
    timeout: '3600', // seconds
    method: 'get',
  },
  {
    id: '9',
    status: 'failed',
    createdAt: new Date(),
    name: 'taskname',
    url: 'https://www.statista.com',
    retries: 2,
    error: 'Error message text...',
    screenshot:
      'https://animashaunmichael.com/wp-content/uploads/2022/05/favicon001-cropped.png',
    visible: true,
    timeout: '3600', // seconds
    method: 'get',
  },
  {
    id: '10',
    status: 'waiting',
    createdAt: new Date(),
    name: 'taskname',
    url: 'https://www.statista.com',
    retries: 2,
    error: '',
    screenshot:
      'https://animashaunmichael.com/wp-content/uploads/2022/05/favicon001-cropped.png',
    visible: true,
    timeout: '3600', // seconds
    method: 'post',
  },
  {
    id: '11',
    status: 'waiting',
    createdAt: new Date(),
    name: 'taskname',
    url: 'https://www.statista.com',
    retries: 2,
    error: 'Error message text...',
    screenshot:
      'https://animashaunmichael.com/wp-content/uploads/2022/05/favicon001-cropped.png',
    visible: true,
    timeout: '3600', // seconds
    method: 'get',
  },
  {
    id: '12',
    status: 'waiting',
    createdAt: new Date(),
    name: 'taskname',
    url: 'https://www.statista.com',
    retries: 2,
    error: '',
    screenshot:
      'https://animashaunmichael.com/wp-content/uploads/2022/05/favicon001-cropped.png',
    visible: true,
    timeout: '3600', // seconds
    method: 'get',
  },
  {
    id: '13',
    status: 'waiting',
    createdAt: new Date(),
    name: 'taskname',
    url: 'https://www.statista.com',
    retries: 2,
    error: 'Error message text...',
    screenshot:
      'https://animashaunmichael.com/wp-content/uploads/2022/05/favicon001-cropped.png',
    visible: true,
    timeout: '3600', // seconds
    method: 'get',
  },
];
