import React, { useState } from 'react';
import { DEFAULT_TABS, WORKFLOW } from 'renderer/dataset';
import { Tabs, Svg } from '@crawless/ui/lite';
import { TabStoreImplementation } from 'renderer/observables';

import { observer } from 'mobx-react';
import Workflow from './workflow';

interface ProjectsRendererProps {
  tabsData: TabStoreImplementation;
}

const ProjectsRenderer = observer(
  ({ tabsData, ...props }: ProjectsRendererProps) => {
    const renderPaneContent = (tab) => {
      if (tab?.type === WORKFLOW) {
        return <Workflow />;
      }
      return <div>Hello World</div>;
    };

    return (
      <Tabs
        size="small"
        onChange={(tab) => tabsData.setActiveTab(tab?.id)}
        // defaultActiveId={tabsData.getActiveTab()}
        activeId={tabsData.getActiveTab()}
      >
        {tabsData.getAllTabs()?.map((el, key) => (
          <Tabs.Pane
            key={key}
            id={el?.id}
            title={
              <div className="d-flex align-items-center pe-2">
                <span className='text-capitalize'>{el?.title}</span>
                {el?.id === tabsData.getActiveTab() && (
                  <Svg.Cross
                    onClick={() => {
                      tabsData.removeTab(el?.id);
                    }}
                    style={{ marginLeft: '10px' }}
                  />
                )}
              </div>
            }
            iconLeft={
              el?.Icon ? (
                <el.Icon style={el?.color ? { fill: el?.color } : {}} />
              ) : null
            }
          >
            {renderPaneContent(el)}
          </Tabs.Pane>
        ))}
      </Tabs>
    );
  }
);

export default ProjectsRenderer;
