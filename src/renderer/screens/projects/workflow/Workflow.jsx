import React from 'react';
import { Tabs, Svg } from '@crawless/ui/lite';
import { Colors } from 'renderer/theme';
import styled from 'styled-components';
import EdgeContent from './EdgeContent';

const Item = styled.div`
  height: 40px;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  color: ${Colors.foreground50};
`;

const Workflow = () => {
  return (
    <Tabs
      style={{ background: '#0000' }}
      panesStyle={{ height: '100%', background: Colors.background70 }}
      suffix={
        <div
          className="d-flex align-items-center"
          style={{ background: Colors.background70 }}
        >
          <Item className="me-4">
            <Svg.Document
              style={{
                fill: Colors.foreground70,
              }}
            />
            <span className="ms-2">Documentation</span>
          </Item>
          <Item className="me-4">
            <Svg.Share
              style={{
                fill: Colors.foreground70,
              }}
            />
            <span className="ms-2">Collaboration</span>
          </Item>
          <Item className="me-4">
            <Svg.Download
              style={{
                fill: Colors.foreground70,
              }}
            />
            <span className="ms-2">Updates</span>
          </Item>
          <Item className="me-4">
            <Svg.Upload
              style={{
                fill: Colors.foreground70,
              }}
            />
            <span className="ms-2">Releases</span>
          </Item>
          <Item className="me-3">
            <Svg.Settings
              style={{
                fill: Colors.foreground70,
              }}
            />
            <span className="ms-2">Settings</span>
          </Item>
        </div>
      }
    >
      <Tabs.Pane
        title="Edge"
        id="edge"
        iconLeft={<Svg.GlobeLines />}
        running={false}
      >
        <EdgeContent />
      </Tabs.Pane>
      <Tabs.Pane title="Local" id="local" iconLeft={<Svg.PC />} running>
        Local
      </Tabs.Pane>
    </Tabs>
  );
};

export default Workflow;
