/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react';
import { Svg, Input, Checkbox } from '@crawless/ui/lite';
import styled from 'styled-components';
import { Colors } from 'renderer/theme';
import { workflowTasksData } from 'renderer/dataset';
import { InnerTable } from './components';
import {
  TabStoreImplementation,
  TaskItem,
  WorkFlowItem,
  WorkflowStoreImplementation,
} from 'renderer/observables';
import dayjs from 'dayjs';
import LocalizedFormat from 'dayjs/plugin/localizedFormat';
import { observer } from 'mobx-react';
dayjs.extend(LocalizedFormat);

/* const Minus = styled.div`
  cursor: pointer;
  border-radius: 3px;
  background: #4d41cb;
  width: 20px;
  height: 20px;
  display: flex;
  align-items: center;
  justify-content: center;
`; */

const Divider = styled.div`
  width: 100%;
  height: 1px;
  background: ${Colors.foreground10};
  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;
  .title {
    position: absolute;
    color: ${Colors.foreground30};
    border: 1px solid ${Colors.foreground10};
    background-color: ${Colors.background50};
    padding: 5px 15px;
    font-size: 11px;
    border-radius: 20px;
  }
`;

const Table = styled.table`
  width: 100%;
  border: 0px solid ${Colors.background10};
  border-collapse: collapse;
  tr.table-header > td {
    border: 1px solid ${Colors.background10};
    border-collapse: collapse;
  }
  td {
    padding: 8px;
    color: ${Colors.foreground50};
    background: ${Colors.background50};
  }
`;

const Button = ({ title, Icon, iconStyle, ...props }) => {
  return (
    <div
      {...props}
      className="d-flex w-100 align-items-center justify-content-between"
    >
      <span style={{ fontSize: '11px' }}>{title || ''}</span>
      {!!Icon && <Icon style={{ fill: Colors.foreground70, ...iconStyle }} />}
    </div>
  );
};

interface SummaryContentProps {
  tabData: TabStoreImplementation;
  workflowData: WorkflowStoreImplementation;
}
const SummaryContent = observer(
  ({ tabData, workflowData, ...props }: SummaryContentProps) => {
    const [data, setData] = useState(workflowTasksData);
    const [workFlowDate, setWorkFlowDate] = useState('');

    const structuriseDataForTable = async (brutData: WorkFlowItem) => {
      let structurisedData = [];
      if (Array.isArray(brutData?.tasks)) {
        brutData?.tasks.map((task: TaskItem) => {
          structurisedData.push({
            ...task,
            status: task?.status || 'waiting',
            url: task?.url || '',
            retries: task?.retries || '',
            error: task?.error || '',
            method: task?.method || '',
            timeout: task?.timeout || '',
            visible: task?.visible || '',
            screenshot:
              task?.screenshot ||
              'https://animashaunmichael.com/wp-content/uploads/2022/05/favicon001-cropped.png',
            // brutData?.image ||
          });
        });
      }
      return structurisedData;
    };

    const fetchData = async () => {
      const activeTabId = tabData.getActiveTab();
      const workflows = workflowData.getWorkflows();
      const activeWorkFlowIndex = workflows.findIndex(
        (el) => el?.id === activeTabId
      );
      if (activeWorkFlowIndex !== -1) {
        setWorkFlowDate(workflows[activeWorkFlowIndex].createdAt);
        setData(await structuriseDataForTable(workflows[activeWorkFlowIndex]));
      } else {
        tabData.removeTab(activeTabId);
      }
    };

    useEffect(() => {
      fetchData();
    }, []);

    return (
      <Table>
        <thead>
          <tr className="table-header">
            <td style={{ width: '35px' }}>
              <Checkbox className="d-flex align-items-center" indeterminate />
            </td>
            <td style={{ minWidth: '80px' }}>
              <Button title="status" Icon={Svg.Filter} />
            </td>
            <td style={{ minWidth: '80px' }}>
              <Button title="added" Icon={Svg.Filter} />
            </td>
            <td style={{ minWidth: '80px' }}>
              <Button title="task" Icon={Svg.Filter} />
            </td>
            <td style={{ minWidth: '150px' }}>
              <Input
                value="url"
                fieldStyle={{
                  border: '0px',
                  background: '#0000',
                  color: Colors.foreground50,
                }}
                iconRight={<Svg.Filter />}
              />
            </td>
            <td style={{ minWidth: '80px' }}>
              <Button title="retries" Icon={Svg.Filter} />
            </td>
            <td style={{ minWidth: '80px' }}>
              <Button title="error" Icon={Svg.Filter} />
            </td>
            <td>
              <Button title="screenshot" Icon={Svg.Filter} />
            </td>
            <td style={{ minWidth: '80px' }}>
              <Button title="visible" Icon={Svg.Filter} />
            </td>
            <td style={{ minWidth: '80px' }}>
              <Button title="timeout" Icon={Svg.Filter} />
            </td>
            <td style={{ minWidth: '80px' }}>
              <Button title="method" Icon={Svg.Filter} />
            </td>
            <td style={{ width: '35px', backgroundColor: Colors.background70 }}>
              <div className="d-flex align-items-center justify-content-center h-100 w-100">
                <Svg.Menu size="small" style={{ fill: Colors.foreground90 }} />
              </div>
            </td>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td colSpan={12}>
              <Divider className="my-4">
                <span className="title">
                  {dayjs(workFlowDate).format('dddd, LL')}
                </span>
              </Divider>
            </td>
          </tr>
          <InnerTable linesData={data} />
        </tbody>
      </Table>
    );
  }
);

export default SummaryContent;
