export default function setupAxios(axios) {
  axios.interceptors.request.use(
    (config) => {
      config.headers.post['Content-Type'] = 'application/json';
      return config;
    },
    (err) => Promise.reject(err)
  );

  axios.interceptors.response.use(
    (response) => {
      return response;
    },
    (error) => {
      return Promise.reject(error?.response?.data);
    }
  );
}
