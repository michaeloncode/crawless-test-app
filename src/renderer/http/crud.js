import axios from 'axios';

export function getWorkflows() {
  return axios.get(
    'https://api.stage.crawless.com/store/workflow/statista?version=0.0.2'
  );
}
