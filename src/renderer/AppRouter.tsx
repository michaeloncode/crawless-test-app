import React from 'react';
import { MemoryRouter as Router, Routes, Route } from 'react-router-dom';
import { TabStore, WorkflowStore } from './observables';

import MainScreen from './screens/MainScreen';

export default function AppRouter() {
  return (
    <Router>
      <Routes>
        <Route
          path="/"
          element={<MainScreen workflow={WorkflowStore} tabsData={TabStore} />}
        />
      </Routes>
    </Router>
  );
}
